- name: Set new hostname
  ansible.builtin.hostname:
    name: "{{ inventory_hostname }}"

- name: Install aptitude
  apt:
    name: aptitude
    state: latest
    update_cache: true

- name: Setup passwordless sudo
  lineinfile:
    path: /etc/sudoers
    state: present
    regexp: "^%sudo"
    line: "%sudo ALL=(ALL) NOPASSWD: ALL"
    validate: "/usr/sbin/visudo -cf %s"

- name: Create a new regular user with sudo privileges
  user:
    name: "{{ item.name }}"
    state: present
    groups: sudo
    append: true
    create_home: true
    shell: /bin/bash
  with_items: "{{ users }}"

- name: Set authorized key for remote user
  ansible.posix.authorized_key:
    user: "{{ item.name }}"
    state: present
    key: "{{ item.key }}"
  with_items: "{{ users }}"

- name: Disable password authentication for root
  lineinfile:
    path: /etc/ssh/sshd_config
    state: present
    regexp: "^#?PermitRootLogin"
    line: "PermitRootLogin prohibit-password"

- name: Update apt and install required system packages
  apt:
    pkg:
      - curl
      - ufw
      - unzip
      - podman
    state: latest
    update_cache: true

# Setup management server
- include_tasks: tasks/management/main.yml
  when: "'management' in inventory_hostname"

# Install tailscale on other nodes and connet to management
- include_tasks: tasks/common/tailscale.yml
- include_tasks: tasks/common/connect.yml

# Setup other nodes
- include_tasks: tasks/compute/main.yml
  when: "'compute' in inventory_hostname"

- include_tasks: tasks/proxy/main.yml
  when: "'proxy' in inventory_hostname"

- include_tasks: tasks/firewall/main.yml