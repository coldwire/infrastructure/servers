datacenter = "dc1"

data_dir = "/opt/consul"

ui_config{
  enabled = true
}

bind_addr   = "{{ GetInterfaceIP \"tailscale0\" }}"
client_addr = "{{ GetInterfaceIP \"tailscale0\" }}"

server = true

bootstrap_expect = 1