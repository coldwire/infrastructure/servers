datacenter = "dc1"

data_dir   = "/opt/nomad"
plugin_dir = "/opt/plugins"

addresses {
  http = "0.0.0.0"
  rpc  = "{{ GetInterfaceIP \"tailscale0\" }}"
  serf = "{{ GetInterfaceIP \"tailscale0\" }}"
} 

advertise {
  http = "{{ GetInterfaceIP \"tailscale0\" }}"
  rpc  = "{{ GetInterfaceIP \"tailscale0\" }}"
  serf = "{{ GetInterfaceIP \"tailscale0\" }}"
}

client {
  enabled = true
  network_interface = "tailscale0"
}

server {
  enabled = true
  bootstrap_expect = 1
}

consul {
  address = "{{ GetInterfaceIP \"tailscale0\" }}:8500"
}

vault {
  enabled = true
  address = "http://compute.backbone.coldnet.org:8200"
}

plugin "nomad-driver-podman" {
  config {
    socket_path = "unix:///var/run/podman/podman.sock"
  }
}