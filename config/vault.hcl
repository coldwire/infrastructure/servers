ui = true

# Storage driver
storage "consul" {
  address = "compute.backbone.coldnet.org:8500"
  path    = "vault"
}

# HTTP listener
listener "tcp" {
  address = "compute.backbone.coldnet.org:8200"
  tls_disable = 1
}