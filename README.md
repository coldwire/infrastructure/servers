# Infrastructure Deploy
## Description

This is a quick way to deploy 

## Develop
Firt run the test network with `vagrant up --no-provision` wait a little bit and then run `vagrant provision`
This might takes a while depending of your network speed, processor, memory, etc.

When everything is OK, just run `vagrant ssh client`, this will create an ssh session into "client" which is the vm that simulate an admin of the network, from here you can acces everything you want :)

## Deploy
### Hosts

Create a `host` file with a content like that:
```
proxy        ansible_host=<address of the proxy server>      ansible_user=root
management   ansible_host=<address of the manager server>    ansible_user=root
compute      ansible_host=<address of the compute server>    ansible_user=root
```

also check the `vars.yml` and `vars.prod.yml` if you want to add more users or set another managing server ^^

Then, just run ansible:
`ansible-playbook main.yml -i hosts -e "@vars.prod.yml`